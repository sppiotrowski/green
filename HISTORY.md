Setup PostgreSQL
================
```sh
# docker hub
open https://hub.docker.com/search?q=postgresql&type=image

# docker cleanup
docker system prune -fa --volumes
# docker rm -f $(docker ps -a -q) && docker rmi -f $(docker images -q)

docker run --name postgres-green -e POSTGRES_PASSWORD=admin -d -p 5432:5432 postgres:11

# install & setup pl/python
docker exec -it postgres-green bash
apt-get update && apt-get install -y --no-install-recommends "postgresql-plpython-$PG_MAJOR"
psql -U postgres -c "CREATE DATABASE green;"
psql -U postgres -d green -c "CREATE EXTENSION plpythonu;"

# access via db uri
postgresql://postgres:admin@10.0.90.24:5432/postgres

# access via qslq client
psql -h 10.0.90.24 -U postgres -d green # admin

# access via docker
docker exec -it postgres-green bash -c "psql -U postgres -d green"
```

